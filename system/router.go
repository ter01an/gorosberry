package system

import (
	"github.com/gin-gonic/gin"
)

var router *gin.Engine

func GET() {
	router.GET("/", func(c *gin.Context){

	})
}

func POST() {
	router.POST("/", func(c *gin.Context){

	})
}

func PUT() {
	router.PUT("/", func(c *gin.Context){

	})
}

func init()  {
	router = gin.Default()
}

func Routing()  {
	GET()
	POST()
	PUT()
}
